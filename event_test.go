package dispatcher

import (
	"context"
	"reflect"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
)

type EventTest struct {
	Field int
}

type EventEmptyTest struct{}

func TestEvent(t *testing.T) {
	var eventFirstResult bool
	var eventSecondResult bool
	var eventThirdResult bool
	var eventFourthResult bool
	var eventFifthResult bool

	eventFirstHandler := func(ctx context.Context, event *EventTest) error {
		assert.Equal(t, 7, event.Field)
		assert.False(t, eventSecondResult)
		assert.False(t, eventThirdResult)
		assert.False(t, eventFourthResult)
		assert.True(t, eventFifthResult)
		eventFirstResult = true
		return nil
	}
	err := RegisterSubscriber(eventFirstHandler, 0)
	assert.Nil(t, err)
	err = RegisterSubscriber(eventFirstHandler, 0)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	err = RegisterSubscriber(eventFirstHandler, 100)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	assert.False(t, eventFirstResult)

	eventSecondHandler := func(ctx context.Context, event *EventTest) error {
		assert.Equal(t, 7, event.Field)
		assert.True(t, eventFirstResult)
		assert.True(t, eventThirdResult)
		assert.False(t, eventFourthResult)
		assert.True(t, eventFifthResult)
		eventSecondResult = true
		return nil
	}
	err = RegisterSubscriber(eventSecondHandler, 10)
	assert.Nil(t, err)
	err = RegisterSubscriber(eventSecondHandler, 0)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	err = RegisterSubscriber(eventSecondHandler, 100)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	assert.False(t, eventSecondResult)

	eventThirdHandler := func(ctx context.Context, event *EventTest) error {
		assert.Equal(t, 7, event.Field)
		assert.True(t, eventFirstResult)
		assert.False(t, eventSecondResult)
		assert.False(t, eventFourthResult)
		assert.True(t, eventFifthResult)
		eventThirdResult = true
		return nil
	}
	err = RegisterSubscriber(eventThirdHandler, 0)
	assert.Nil(t, err)
	err = RegisterSubscriber(eventThirdHandler, 0)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	err = RegisterSubscriber(eventThirdHandler, 100)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	assert.False(t, eventThirdResult)

	eventFourthHandler := func(ctx context.Context, event *EventTest) error {
		assert.Equal(t, 7, event.Field)
		assert.True(t, eventFirstResult)
		assert.True(t, eventSecondResult)
		assert.True(t, eventThirdResult)
		assert.True(t, eventFifthResult)
		eventFourthResult = true
		return nil
	}
	err = RegisterSubscriber(eventFourthHandler, 10)
	assert.Nil(t, err)
	err = RegisterSubscriber(eventFourthHandler, 0)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	err = RegisterSubscriber(eventFourthHandler, 100)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	assert.False(t, eventFourthResult)

	eventFifthHandler := func(ctx context.Context, event *EventTest) error {
		assert.Equal(t, 7, event.Field)
		assert.False(t, eventFirstResult)
		assert.False(t, eventSecondResult)
		assert.False(t, eventThirdResult)
		assert.False(t, eventFourthResult)
		eventFifthResult = true
		return nil
	}
	err = RegisterSubscriber(eventFifthHandler, -10)
	assert.Nil(t, err)
	err = RegisterSubscriber(eventFifthHandler, 0)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	err = RegisterSubscriber(eventFifthHandler, 100)
	assert.ErrorIs(t, err, ErrSubscriberAlreadyRegistered)
	assert.False(t, eventFifthResult)

	root, _ := container.Load(getIdForObject(new(EventTest)))
	assert.NotNil(t, root)
	chain, ok := root.(*subscriber)
	assert.True(t, ok)
	assert.NotNil(t, chain.next)
	assert.NotNil(t, chain.next.next)
	assert.NotNil(t, chain.next.next.next)
	assert.NotNil(t, chain.next.next.next.next)
	assert.Nil(t, chain.next.next.next.next.next)
	getNames := func(f []any) (result []string) {
		for _, handler := range f {
			result = append(result, runtime.FuncForPC(reflect.ValueOf(handler).Pointer()).Name())
		}
		return result
	}

	assert.Equal(t, getNames([]any{eventFifthHandler, eventFirstHandler, eventThirdHandler, eventSecondHandler, eventFourthHandler}), getNames([]any{chain.handler, chain.next.handler, chain.next.next.handler, chain.next.next.next.handler, chain.next.next.next.next.handler}))

	err = Dispatch(context.Background(), &EventTest{Field: 7})
	assert.Nil(t, err)
	assert.True(t, eventFirstResult)
	assert.True(t, eventSecondResult)
	assert.True(t, eventThirdResult)
	assert.True(t, eventFourthResult)
	assert.True(t, eventFifthResult)

	err = Dispatch(context.Background(), new(EventEmptyTest))
	assert.Nil(t, err)
}
