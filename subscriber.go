package dispatcher

import "sync"

var subscriberMutex sync.Mutex

func newSubscriber(handler any, priority int) *subscriber {
	return &subscriber{
		handler:  handler,
		priority: priority,
	}
}

type subscriber struct {
	handler    any
	priority   int
	prev, next *subscriber
}

func (s *subscriber) chain(handler any, priority int) *subscriber {
	subscriberMutex.Lock()
	defer subscriberMutex.Unlock()
	e := newSubscriber(handler, priority)
	if s == nil {
		return e
	}
	root := s
	for {
		if root.prev != nil {
			root = root.prev
		} else {
			break
		}
	}

	if root.priority > e.priority {
		e.next = root
		root.prev = e
		return e
	}

	current := root
	for {
		if current.next == nil || current.next.priority > e.priority {
			e.next = current.next
			e.prev = current
			if current.next != nil {
				current.next.prev = e
			}
			current.next = e
			break
		} else {
			current = current.next
		}
	}
	return root
}
