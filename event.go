package dispatcher

import (
	"context"
	"fmt"
	"reflect"
	"runtime"
	"sync"
)

var ErrSubscriberAlreadyRegistered = fmt.Errorf("subscriber has been already registered")

var (
	container sync.Map
	hash      sync.Map
)

func RegisterSubscriber[Event any](handler func(ctx context.Context, event *Event) error, priority int) error {
	eventCode := getIdForObject(new(Event))
	subscriberCode := runtime.FuncForPC(reflect.ValueOf(handler).Pointer()).Name()
	code := eventCode + "::" + subscriberCode
	if _, found := hash.Load(code); found {
		return ErrSubscriberAlreadyRegistered
	}
	hash.Store(code, true)

	if chain, found := container.Load(eventCode); found {
		container.Store(eventCode, chain.(*subscriber).chain(handler, priority))
	} else {
		container.Store(eventCode, newSubscriber(handler, priority))
	}
	return nil
}

func Dispatch[Event any](ctx context.Context, event *Event) error {
	eventCode := getIdForObject(event)

	if root, found := container.Load(eventCode); found {
		chain := root.(*subscriber)
		for {
			err := chain.handler.(func(context.Context, *Event) error)(ctx, event)
			if err != nil {
				return err
			}
			if chain.next == nil {
				break
			}
			chain = chain.next
		}
	}
	return nil
}

func getIdForObject(object any) string {
	return fmt.Sprintf("%T", object)
}
